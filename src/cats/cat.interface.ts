import { Int, ObjectType, Field, InputType, ID } from '@nestjs/graphql'
import { Document } from 'mongoose'

@InputType()
export class CatInput {
  @Field()
  readonly name!: string
  @Field((type) => Int)
  readonly age!: number
  @Field()
  readonly breed!: string
}

@ObjectType()
export class CatType {
  @Field((type) => ID)
  id!: string
  @Field()
  readonly name!: string
  @Field((type) => Int)
  readonly age!: number
  @Field()
  readonly breed!: string
}

export interface Cat extends Document {
  readonly name: string
  readonly age: string
  readonly breed: string
}
