import { CatService } from './cat.service'
import { CatSchema } from './cat.schema'
import { MongooseModule } from '@nestjs/mongoose'
import { CatResolver } from './cat.resolver'
import { Module } from '@nestjs/common'

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Cat', schema: CatSchema }])],
  providers: [CatResolver, CatService],
})
export class CatModules {}
