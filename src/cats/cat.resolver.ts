import { CatType, CatInput } from './cat.interface'
import { CatService } from './cat.service'
import { Resolver, Query, Mutation, Args } from '@nestjs/graphql'

@Resolver()
export class CatResolver {
  constructor(private readonly service: CatService) {}

  @Query(() => String)
  async hello() {
    return 'Hello, World!'
  }

  @Query(() => [CatType])
  async cats() {
    return this.service.findAll()
  }

  @Mutation(() => CatType)
  async create(@Args('input') input: CatInput) {
    return this.service.create(input)
  }
}
