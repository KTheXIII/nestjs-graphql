import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { Cat, CatType, CatInput } from './cat.interface'

@Injectable()
export class CatService {
  constructor(@InjectModel('Cat') private readonly model: Model<Cat>) {}

  async create(cat: CatInput): Promise<Cat> {
    const createdCat = new this.model(cat)
    return await createdCat.save()
  }

  async findAll(): Promise<Cat[]> {
    return await this.model.find().exec()
  }
}
