import path from 'path'
import { CatModules } from './cats/cat.module'
import { Module } from '@nestjs/common'
import { AppService } from './app.service'
import { AppController } from './app.controller'
import { MongooseModule } from '@nestjs/mongoose'
import { GraphQLModule } from '@nestjs/graphql'

const ROOT_DIR = path.resolve()

@Module({
  imports: [
    CatModules,
    GraphQLModule.forRoot({
      autoSchemaFile: path.join(ROOT_DIR, 'data', 'schema.gql'),
    }),
    MongooseModule.forRoot(`mongodb://127.0.0.1:27017`, {
      useNewUrlParser: true,
      auth: { user: 'miku', password: 'hatsune' },
      db: 'mikudb',
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
