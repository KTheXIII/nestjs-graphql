# NestJS with GraphQL

Learning NestJS with GraphQL.

MongoDB as the database server.

## How to run

### Requirements
- nodejs
- docker

### Development

Start docker container
```
docker-compose up
```

Start server on PORT `3000`

```
yarn start:dev
```